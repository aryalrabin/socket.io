/**
 * Created by raryal on 4/1/2015.
 */

import java.lang.reflect.Constructor

import controllers.CustomRoutesService
import play.api._
import play.api.db.slick.Config.driver.profile.simple._
import plugins.{LoginUserViewTemplate, DatabaseAccess, ConnectionInfos}
import securesocial.core.providers.GoogleProvider
import securesocial.core.RuntimeEnvironment
import service.{MyEventListener, InMemoryUserService, DemoUser}

import scala.collection.immutable.ListMap

object Global extends GlobalSettings with DatabaseAccess {

  override def onStart(app: Application): Unit = {
    super.onStart(app)
    try {
      database withSession { implicit session =>
        ConnectionInfos.ddl.create
      }
    } catch {
      case e: Throwable => Logger.error(s"Error during connectionInfos.ddl.create: ${e.getMessage}")
    }
  }

  /**
   * The runtime environment for this sample app.
   */
  object MyRuntimeEnvironment extends RuntimeEnvironment.Default[DemoUser] {
    override lazy val routes = new CustomRoutesService()
    override lazy val viewTemplates: LoginUserViewTemplate = new LoginUserViewTemplate(this)
    override lazy val userService: InMemoryUserService = new InMemoryUserService()
    override lazy val eventListeners = List(new MyEventListener())

    override lazy val providers = ListMap(
      // oauth 2 client providers
      include(new GoogleProvider(routes, cacheService, oauth2ClientFor(GoogleProvider.Google)))
    )
  }

  /**
   * An implementation that checks if the controller expects a RuntimeEnvironment and
   * passes the instance to it if required.
   *
   * This can be replaced by any DI framework to inject it differently.
   *
   * @param controllerClass
   * @tparam A
   * @return
   */
  override def getControllerInstance[A](controllerClass: Class[A]): A = {
    val instance = controllerClass.getConstructors.find { c =>
      val params = c.getParameterTypes
      params.length == 1 && params(0) == classOf[RuntimeEnvironment[DemoUser]]
    }.map {
      _.asInstanceOf[Constructor[A]].newInstance(MyRuntimeEnvironment)
    }
    instance.getOrElse(super.getControllerInstance(controllerClass))
  }
}
