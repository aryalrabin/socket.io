package plugins

import com.originate.play.websocket.ClientConnection
import com.originate.play.websocket.socketio._
import com.originate.play.websocket.socketio.plugins.SocketIoHooksPlugin
import play.api.Logger
import play.api.db.slick.Config.driver.profile.simple._
import play.api.libs.json.{Json, JsNull, JsValue}

/**
 * Created by raryal on 4/1/2015.
 */
class SocketIoHooksPluginImpl (val app: play.Application)
  extends SocketIoHooksPlugin  with DatabaseAccess {
  def packetReceivedHook: (ClientConnection, SocketIoPacket) => Unit = {
    (connection, packet) =>
      Logger.info(s"SocketIoHooksPlugin: received packet")
      database withSession { implicit session =>
        ConnectionInfos.filter(_.connectionId === connection.connectionId).map(_.lastRequestTimestamp)
          .update(System.currentTimeMillis())
      }

      val packetOpt = packet match {
        case p: Message =>
          Some(p.copy(messageId = "", ack = Acknowledge.No))
        case p: JsonMessage =>
          Some(p.copy(messageId = "", ack = Acknowledge.No))
        case p: Event =>
          Some(p.copy(messageId = "", ack = Acknowledge.No))
        case _ =>
          Logger.info(s"SocketIoHooksPlugin: Packet doesn't need to be sent: $packet")
          None
      }

      packetOpt map {
        packetToSent => database withSession { implicit session =>
          val q = ConnectionInfos.filter(_.connectionId =!= connection.connectionId)

          q.list.toSet foreach {
            connectionInfo: ConnectionInfo =>
              SocketIoSender.send(connectionInfo.connectionId, packetToSent)
          }
        }
      }

      if (packet.isAckRequested) {
          packet match {
           case p: Event =>
              Logger.info(s"SocketIoHooksPlugin: Sent Message back: ${p}")
//              SocketIoSender.send(connection.connectionId,  Ack(p.messageId, p.data))
              SocketIoSender.send(connection.connectionId, Event(json = toJson(p.data.replace("json", "message_sent"))))
           case _ =>
             None
          }
      }

  }

  def toJson(data: String): JsValue = if (data == null || data.trim.isEmpty) JsNull else Json.parse(data)
}
