package plugins

import com.originate.play.websocket.{ClientInfo, ClientConnection}
import com.originate.play.websocket.plugins.ConnectionRegistrarPlugin
import play.api.Logger
import play.api.db.slick.Config.driver.profile.simple._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import play.api.cache.Cache
import play.api.Play.current

/**
 * Created by raryal on 4/1/2015.
 */
class ConnectionRegistrarPluginImpl(val app: play.Application) extends ConnectionRegistrarPlugin with DatabaseAccess {

  def register(connection: ClientConnection): Future[Unit] = {
    Logger.info(s"ConnectionRegistrarPlugin: register: $connection")
    Future {
      database withSession { implicit session =>
        ConnectionInfos += ConnectionInfo(
          connection.connectionId,
          connection.clientInfo.clientId,
          connection.clientInfo.userId,
          connection.connectionActorUrl,
          System.currentTimeMillis())
      }
    }
  }

  def deregister(connection: ClientConnection): Future[Unit] = {
    Logger.info(s"ConnectionRegistrarPlugin: deregister: $connection")

    Future {
      database withSession { implicit session =>
        ConnectionInfos.filter(_.connectionId === connection.connectionId).delete
      }
    }
  }

  def find(connectionId: String): Future[Option[ClientConnection]] = {
    Logger.info(s"ConnectionRegistrarPlugin: find $connectionId")

    Future {
      val clientConnectionOpt = database withSession { implicit session =>
        ConnectionInfos.filter(_.connectionId === connectionId).list.headOption
      } map {
        connectionInfo =>
          ClientConnection(
            ClientInfo(connectionInfo.userId, connectionInfo.clientId),
            connectionInfo.connectionId,
            connectionInfo.connectionActorUrl)
      }
      Logger.info(s"ConnectionRegistrarPlugin: found $clientConnectionOpt")
      clientConnectionOpt
    }
  }
}
