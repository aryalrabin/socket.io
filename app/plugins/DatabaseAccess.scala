package plugins

/**
 * Created by raryal on 4/1/2015.
 */
import play.api.Play.current
import play.api.db.slick.Database

trait DatabaseAccess {
  lazy val database = Database()
}
