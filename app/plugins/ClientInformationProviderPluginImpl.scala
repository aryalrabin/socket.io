package plugins

/**
 * Created by raryal on 4/1/2015.
 */

import java.util.concurrent.atomic.AtomicLong

import com.originate.play.websocket.plugins.ClientInformationProviderPlugin
import models.UserInfo
import play.api.mvc.RequestHeader
import com.originate.play.websocket.ClientInfo
import play.api.Logger
import play.api.cache.Cache
import play.api.Play.current

class ClientInformationProviderPluginImpl(val app: play.Application)
  extends ClientInformationProviderPlugin {

  val userId = new AtomicLong(0)

  def getClientInfo(implicit request: RequestHeader): Option[ClientInfo] = {
    Logger.info(s"${getClass.getSimpleName}: ClientInfo requested")
    request.session.get("s") map (sessionId => {
     val maybeUser: Option[UserInfo] = Cache.getAs[UserInfo](sessionId)

      val username = maybeUser match{
        case Some(user) => user.userId
        case None => throw new IllegalStateException("Exception thrown")
      }
      ClientInfo(username, sessionId)
    })
  }

}
