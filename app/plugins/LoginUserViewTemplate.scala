package plugins

import play.api.data.Form
import play.api.i18n.Lang
import play.api.mvc.RequestHeader
import play.twirl.api.Html
import securesocial.controllers.{ChangeInfo, RegistrationInfo, ViewTemplates}
import securesocial.core.RuntimeEnvironment


/**
 * Created by rabin on 14-11-01.
 */
class LoginUserViewTemplate(env: RuntimeEnvironment[_]) extends ViewTemplates {

  implicit val implicitEnv = env

  override def getLoginPage(form: Form[(String, String)],
                            msg: Option[String] = None)(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.login(form, msg)(request, lang, env)
  }

  override def getSignUpPage(form: Form[RegistrationInfo], token: String)(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.Registration.signUp(form, token)(request, lang, env)
  }

  override def getStartSignUpPage(form: Form[String])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.Registration.startSignUp(form)(request, lang, env)
  }

  override def getStartResetPasswordPage(form: Form[String])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.Registration.startResetPassword(form)(request, lang, env)
  }

  override def getResetPasswordPage(form: Form[(String, String)], token: String)(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.Registration.resetPasswordPage(form, token)(request, lang, env)
  }

  override def getPasswordChangePage(form: Form[ChangeInfo])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.passwordChange (form)(request, lang, env)
  }

  def getNotAuthorizedPage(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.notAuthorized()(request, lang, env)
  }
}
