package plugins

/**
 * Created by raryal on 4/1/2015.
 */
import org.joda.time.DateTime
import play.api.db.slick.Config.driver.profile.simple._

case class ConnectionInfo(
                           connectionId: String,
                           clientId: String,
                           userId: String,
                           connectionActorUrl: String,
                           lastRequestTimestamp: Long) {
  val lastRequestTime: DateTime = new DateTime(lastRequestTimestamp)
}

class ConnectionInfos(tag: Tag) extends Table[ConnectionInfo](tag, "connection_infos") {
  def connectionId = column[String]("connection_id", O.PrimaryKey)

  def clientId = column[String]("client_id")

  def userId = column[String]("user_id")

  def connectionActorUrl = column[String]("connection_actor_url")

  def lastRequestTimestamp = column[Long]("last_request_timestamp")

  def * = (connectionId, clientId, userId, connectionActorUrl, lastRequestTimestamp) <>
    (ConnectionInfo.tupled, ConnectionInfo.unapply)
}

object ConnectionInfos extends TableQuery(new ConnectionInfos(_))
