package models

/**
 * Created by raryal on 4/15/2015.
 */
case class UserInfo (firstName: String, lastName: String, userId: String, sessionId: String)
