package controllers

import java.util.UUID

import models.UserInfo
import securesocial.core._
import service.DemoUser
import play.api._
import play.api.cache.Cache
import play.api.Play.current

import plugins.DatabaseAccess

class Application(override implicit val env: RuntimeEnvironment[DemoUser]) extends securesocial.core.SecureSocial[DemoUser] with DatabaseAccess{

  def index = SecuredAction {
    implicit request =>

      val session = request.session.get("s") match {
        case Some(sessionId) => {
          val userInfo : UserInfo =  new UserInfo(request.user.main.firstName.getOrElse(""), request.user.main.lastName.getOrElse(""), request.user.main.userId, sessionId)
          Cache.set(sessionId, userInfo)
          request.session
        }
        case _ =>
          val sessionId = UUID.randomUUID().toString
          Logger.info("Session created: " + sessionId)
          val userInfo : UserInfo =  new UserInfo(request.user.main.firstName.getOrElse(""), request.user.main.lastName.getOrElse(""), request.user.main.userId, sessionId)
          Cache.set(sessionId, userInfo)
          request.session +("s", sessionId)
      }

      Ok(views.html.index("Your new application is ready.")).withSession(session)
  }

}