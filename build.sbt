name := """socket-io"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.2"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
   cache,
  "com.typesafe.play" %% "play-slick" % "0.8.0",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "com.typesafe.akka" %% "akka-remote" % "2.3.5",
  "com.originate" %% "play2-websocket" % "1.0.5",
  "ws.securesocial" %% "securesocial" % "master-SNAPSHOT"
)


